//Q1. Write a program to input n numbers on command line argument and calculate maximum of them.

package P1;

public class Assignment_1 {
	public static int calcMax(int arr[]) {
		int max = arr[0];
		for(int i=0; i<arr.length-1; i++) {
			if(max<arr[i+1]){
				max = arr[i+1];
			}
		}
		return max;
	}
	public static void main(String[] args) {
		int arr[] = new int[10];
		for(int i = 0; i< args.length; i++) {
			arr[i] = Integer.parseInt(args[i]);
		}
		System.out.println("Array you have entered : ");
		for(int a:arr) {
			System.out.print(a+"  ");
		}
		System.out.println("\nMaximum : "+Assignment_1.calcMax(arr));
		
	}

}
