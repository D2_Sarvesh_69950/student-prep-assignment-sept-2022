//Q3. Write a program to calculate Fibonacci Series up to n numbers
package P1;

import java.util.Scanner;

public class Assignment_3 {
	
	public static void fibonacci(int num) {
		
		int fib1 = 0;
		int fib2 = 1;
		int fibNext = 0;
		for(int i =1; i<=num; i++) {
			System.out.print(fib1+" ");
			fibNext = fib1 + fib2;
			fib1 = fib2;
			fib2 = fibNext;
		}
	}
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		int num;
		System.out.println("Enter a number : ");
		num = sc.nextInt();
		Assignment_3.fibonacci(num);
	}

}
