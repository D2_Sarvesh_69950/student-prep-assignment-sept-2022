//Write a program to calculate a Factorial of a number.
package P1;
import java.util.Scanner;

public class Assignment_2 {
	
	public static long calcFact(int num) {
		long fact = 1;
		for(int i=1;i<=num;i++)
			fact*=i;
		return fact;
	}
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		int num;
		System.out.println("Enter a number : ");
		num = sc.nextInt();
		System.out.println("Factorial : ("+num+")! = "+Assignment_2.calcFact(num));
		
	}

}
